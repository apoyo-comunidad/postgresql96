# postgresql96

## PASO 1
docker build -t psql96 {PATH}
## PASO 2
docker run -it -d -p 5432:5432 --restart=always --name cont_psql96 psql96 bash
## PASO 3
Conectarse desde el gestor de BD con usuario postgres y clave postgres, host= 0.0.0.0
## PASO 4
Ejecutar en la maquina local: sudo apt-get install -y postgresql-client
para poder ejecutar la consola sin instalar el postgresql, sino solo para 
llamar a los hosts remotos o contenedores
## PASO 5
Por ahora cada vez que te conectes, ingresa con docker attach cont_psql96 e inicia el servicio