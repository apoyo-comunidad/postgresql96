##############################################################
##   Instalacion de postgresql 9.6 sobre ubuntu 16.04 LTS   ##
##############################################################
FROM ubuntu:16.04
LABEL locopump augusto.caceres.puma@gmail.com
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update -y 
RUN apt-get install -y --no-install-recommends apt-utils
RUN apt-get update -y
RUN apt-get install -y software-properties-common
RUN apt-get update -y
RUN apt-get install -y wget
RUN apt-get update -y
RUN touch /etc/apt/sources.list.d/pgdg.list
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" >> /etc/apt/sources.list.d/pgdg.list
RUN add-apt-repository "deb http://apt.postgresql.org/pub/repos/apt/ xenial-pgdg main" -y
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - && \
    apt-get update -y && \
    apt-get install postgresql-9.6 -y && \
    apt-get update -y
RUN echo "listen_addresses = '*'" >> /etc/postgresql/9.6/main/postgresql.conf
RUN sed -i "85d" /etc/postgresql/9.6/main/pg_hba.conf && \
    sed -i "89d" /etc/postgresql/9.6/main/pg_hba.conf && \
    sed -i "90d" /etc/postgresql/9.6/main/pg_hba.conf && \
    sed -i -e "/^ssl.*$/d" /etc/postgresql/9.6/main/postgresql.conf && \
    echo "local   all             postgres                                trust" >> /etc/postgresql/9.6/main/pg_hba.conf && \
    echo "local   all             all                                     trust" >> /etc/postgresql/9.6/main/pg_hba.conf && \
    echo "host    all             all             127.0.0.1/32            trust" >> /etc/postgresql/9.6/main/pg_hba.conf && \
    echo "host    all             all             0.0.0.0/24              trust" >> /etc/postgresql/9.6/main/pg_hba.conf && \
    echo "host    all             all             172.17.0.1/24              trust" >> /etc/postgresql/9.6/main/pg_hba.conf
RUN service postgresql restart
VOLUME ["/etc/postgresql", "/var/log/postgresql", "var/lib/postgresql"]
USER postgres
RUN /etc/init.d/postgresql start
EXPOSE 5432
ENV DEBIAN_FRONTEND teletype
CMD ["/usr/lib/postgresql/9.6/bin/postgres --config-file=/etc/postgresql/9.6/main/postgresql.conf"]
CMD ["/bin/bash"]